// Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef FILTER_H__
#define FILTER_H__

#include "touch_noise_filter/touch_noise_filter.h"

#include <linux/input.h>
#include <sys/types.h>

namespace touch_noise_filter {

#define TOUCH_NOISE_MAX_SLOTS 64

#define Log(format, ...) \
  TouchNoiseFilterLog(TOUCH_NOISE_LOG_INFO, "INFO:%s:%d:" format "\n", \
                      __FILE__, __LINE__, ## __VA_ARGS__)
#define Err(format, ...) \
  TouchNoiseFilterLog(TOUCH_NOISE_LOG_ERROR, "ERROR:%s:%d:" format "\n", \
                      __FILE__, __LINE__, ## __VA_ARGS__)

// Data types:

struct Finger {
  Finger()
      : x_pos_(0.0), y_pos_(0.0), tracking_id_(-1), canceled_(false) {}
  __s32 x_pos_, y_pos_;
  __s32 tracking_id_;

  bool canceled_:1;  // set to 1 to mark as noise
};

struct Frame {
  Frame() : timestamp_(0) {}
  double timestamp_;
  Finger fingers_[TOUCH_NOISE_MAX_SLOTS];
};

// Interface for all filters

class NoiseFilter {
public:
  virtual ~NoiseFilter() {};
  virtual void FilterFrame(Frame* previous, Frame* current,
                           size_t num_slots) = 0;
};

}  // namespace touch_noise_filter

#endif  // FILTER_H__
