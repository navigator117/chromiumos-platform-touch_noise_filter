// Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "touch_noise_filter/horizontally_aligned_filter.h"

#include <base/basictypes.h>

#include <algorithm>
#include <cstddef>
#include <cstdio>

namespace touch_noise_filter {

namespace {

// The maximum horizontal distance between touches considered aligned.
__s32 kMaxDistance = 3;

}  // namespace {}

void HorizontallyAlignedFilter::FilterFrame(Frame* previous, Frame* current,
                                            size_t num_slots) {
  for (size_t slot = 0; slot < num_slots; slot++) {
    Finger* cur = &current->fingers_[slot];
    Finger* prev = &previous->fingers_[slot];

    bool arrived = prev->tracking_id_ == -1 && cur->tracking_id_ >= 0;
    if (!arrived)
      continue;

    // Check if within kMaxDistance of an existing touch.
    for (size_t i = 0; i < num_slots; i++) {
      if (i == slot || current->fingers_[i].tracking_id_ == -1)
        continue;
      if (std::abs(current->fingers_[i].x_pos_ - cur->x_pos_) <=
          kMaxDistance) {
        Log("Cancel tracking id %d, down at %f at %d,%d near touch %d at %d,%d",
            cur->tracking_id_, current->timestamp_, cur->x_pos_, cur->y_pos_,
            current->fingers_[i].tracking_id_, current->fingers_[i].x_pos_,
            current->fingers_[i].y_pos_);
        cur->canceled_ = true;
      }
    }
  }
}

}  // namespace touch_noise_filter
