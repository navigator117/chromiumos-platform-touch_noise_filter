// Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef TOUCH_NOISE_FILTER_H__
#define TOUCH_NOISE_FILTER_H__

#include <stdint.h>

// logging interface
#define TOUCH_NOISE_LOG_ERROR 0
#define TOUCH_NOISE_LOG_INFO 1

#ifdef __cplusplus
extern "C" {
#endif  // __cplusplus

// Public API

// Clients must implement this function. This library will call it to do
// logging.
void TouchNoiseFilterLog(int verb, const char* format, ...)
    __attribute__((format(printf, 2, 3)));

void* NewXFilter();
void FreeXFilter(void* x_filter);

void XFilterHandleInputEvent(void* x_filter, const struct input_event* ev);
// *slots_mask |= 1 << slot, for each slot that has been canceled
void XFilterGetCanceledTouches(void* x_filter, uint64_t* slots_mask);

#ifdef __cplusplus
}  // extern "C"
#endif  // __cplusplus

#endif  // TOUCH_NOISE_FILTER_H__
