// Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "touch_noise_filter/touch_noise_filter.h"

#include <base/basictypes.h>

#include "touch_noise_filter/far_apart_taps_filter.h"
#include "touch_noise_filter/filter.h"
#include "touch_noise_filter/horizontally_aligned_filter.h"
#include "touch_noise_filter/input_event_filter.h"
#include "touch_noise_filter/mux_filter.h"
#include "touch_noise_filter/single_frame_tap_filter.h"
#include "touch_noise_filter/single_position_filter.h"

using touch_noise_filter::FarApartTapsFilter;
using touch_noise_filter::HorizontallyAlignedFilter;
using touch_noise_filter::InputEventFilter;
using touch_noise_filter::MuxFilter;
using touch_noise_filter::NoiseFilter;
using touch_noise_filter::SingleFrameTapFilter;
using touch_noise_filter::SinglePositionFilter;

extern "C" {

void* NewXFilter() {
  Log("In NewXFilter");
  NoiseFilter* filters[] = {
    new FarApartTapsFilter,
    new HorizontallyAlignedFilter,
    new SinglePositionFilter
  };
  void* ret = new InputEventFilter(new MuxFilter(filters, arraysize(filters)));
  Log("Exiting NewXFilter");
  return ret;
}

void FreeXFilter(void* x_filter) {
  InputEventFilter* filter = reinterpret_cast<InputEventFilter*>(x_filter);
  delete filter;
}

void XFilterHandleInputEvent(void* x_filter, const struct input_event* ev) {
  InputEventFilter* filter = reinterpret_cast<InputEventFilter*>(x_filter);
  filter->HandleInputEvent(ev);
}

// *slots_mask |= 1 << slot, for each slot that has been canceled
void XFilterGetCanceledTouches(void* x_filter, uint64_t* slots_mask) {
  InputEventFilter* filter = reinterpret_cast<InputEventFilter*>(x_filter);
  filter->GetCanceledTouches(slots_mask);
}

}  // extern "C"
