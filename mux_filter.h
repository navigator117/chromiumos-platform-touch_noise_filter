// Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef MUX_FILTER_H__
#define MUX_FILTER_H__

#include "touch_noise_filter/filter.h"

#include <base/memory/scoped_ptr.h>

namespace touch_noise_filter {

class MuxFilter : public NoiseFilter {
public:
  MuxFilter(NoiseFilter** filters, size_t num_filters);
  virtual ~MuxFilter();
  void FilterFrame(Frame* previous, Frame* current, size_t num_slots);
private:
  scoped_ptr<NoiseFilter*[]> filters_;
  size_t num_filters_;
};

}  // namespace touch_noise_filter

#endif  // MUX_FILTER_H__
