# Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

OBJDIR = obj

OBJECTS=\
	$(OBJDIR)/far_apart_taps_filter.o \
	$(OBJDIR)/horizontally_aligned_filter.o \
	$(OBJDIR)/input_event_filter.o \
	$(OBJDIR)/mux_filter.o \
	$(OBJDIR)/single_frame_tap_filter.o \
	$(OBJDIR)/single_position_filter.o \
	$(OBJDIR)/touch_noise_filter.o

MAINOBJ=\
	$(OBJDIR)/main.o

EXE=main

SONAME=$(OBJDIR)/libtouchnoisefilter.so.0

DEPDIR = .deps

DESTDIR = .

CXXFLAGS+=\
	-g \
	-fno-exceptions \
	-fno-strict-aliasing \
	-fPIC \
	-std=gnu++11 \
	-Wall \
	-Wclobbered \
	-Wempty-body \
	-Werror \
	-Wignored-qualifiers \
	-Wmissing-field-initializers \
	-Wmissing-format-attribute \
	-Wmissing-noreturn \
	-Wsign-compare \
	-Wtype-limits \
	-D__STDC_FORMAT_MACROS=1 \
	-D_FILE_OFFSET_BITS=64 \
	-I..

PKG_CONFIG ?= pkg-config
BASE_VER ?= 307740
PC_DEPS += libchrome-$(BASE_VER)
PC_CFLAGS := $(shell $(PKG_CONFIG) --cflags $(PC_DEPS))
PC_LIBS := $(shell $(PKG_CONFIG) --libs $(PC_DEPS))

CXXFLAGS += $(PC_CFLAGS)
LDFLAGS += $(PC_LIBS)

all: $(SONAME) $(EXE)

$(SONAME): $(OBJECTS)
	$(CXX) -shared -o $@ $(OBJECTS) -Wl,-h$(SONAME:$(OBJDIR)/%=%) \
		$(LDFLAGS)

$(EXE): $(OBJECTS) $(MAINOBJ)
	$(CXX) -o $@ $(OBJECTS) $(MAINOBJ) $(LDFLAGS)

$(OBJDIR)/%.o : %.cc
	mkdir -p $(OBJDIR) $(DEPDIR) || true
	$(CXX) $(CXXFLAGS) -MD -c -o $@ $<
	@mv $(@:$.o=$.d) $(DEPDIR)

LIBDIR ?= /usr/lib

install: $(SONAME)
	install -D -m 0755 $(SONAME) \
		$(DESTDIR)$(LIBDIR)/$(SONAME:$(OBJDIR)/%=%)
	ln -s $(SONAME:$(OBJDIR)/%=%) \
		$(DESTDIR)$(LIBDIR)/$(SONAME:$(OBJDIR)/%.0=%)
	install -D -m 0644 touch_noise_filter.h \
		$(DESTDIR)/usr/include/touch_noise_filter/touch_noise_filter.h

clean:
	rm -rf $(OBJDIR) $(DEPDIR) $(EXE) $(MAINOBJ)
