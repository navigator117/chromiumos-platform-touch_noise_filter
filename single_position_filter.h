// Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef SINGLE_POSITION_FILTER_H__
#define SINGLE_POSITION_FILTER_H__

#include "touch_noise_filter/filter.h"

namespace touch_noise_filter {

#define SINGLE_POSITION_MAX_TOUCHES 100

class SinglePositionFilter : public NoiseFilter {
public:
  SinglePositionFilter();
  virtual ~SinglePositionFilter() {}
  void FilterFrame(Frame* previous, Frame* next, size_t num_slots);

private:
  void StopTrackingTouch(size_t index);
  bool TrackTouch(size_t slot, Frame* frame);

  struct Touch {
    Touch()
        : x_pos_(0.0), y_pos_(0.0),
          slot_(0), end_(0), tracking_id_(-1) {}
    __s32 x_pos_, y_pos_;
    size_t slot_;
    double begin_;
    double end_;
    __s32 tracking_id_;
  };

  // A mapping of slot to tracked touch index in tracked_touches_.
  size_t tracked_slots_[TOUCH_NOISE_MAX_SLOTS];

  // A circular buffer of tracked touches [touches_start_, touches_end_).
  Touch tracked_touches_[SINGLE_POSITION_MAX_TOUCHES];
  size_t touches_start_;
  size_t touches_end_;
};

}  // namespace touch_noise_filter

#endif  // SINGLE_POSITION_FILTER_H__
