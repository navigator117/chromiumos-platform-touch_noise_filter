// Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "touch_noise_filter/mux_filter.h"

#include <algorithm>

namespace touch_noise_filter {

MuxFilter::MuxFilter(NoiseFilter** filters, size_t num_filters)
    : filters_(new NoiseFilter*[num_filters]), num_filters_(num_filters) {
  std::copy(&filters[0], &filters[num_filters], filters_.get());
}

MuxFilter::~MuxFilter() {
  for (size_t i = 0; i < num_filters_; i++)
    delete filters_[i];
}

void MuxFilter::FilterFrame(Frame* previous, Frame* next, size_t num_slots) {
  for (size_t i = 0; i < num_filters_; i++)
    filters_[i]->FilterFrame(previous, next, num_slots);
}

}  // namespace touch_noise_filter
