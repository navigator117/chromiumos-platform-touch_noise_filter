// Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef HORIZONTALLY_ALIGNED_FILTER_H__
#define HORIZONTALLY_ALIGNED_FILTER_H__

#include "touch_noise_filter/filter.h"

namespace touch_noise_filter {

class HorizontallyAlignedFilter : public NoiseFilter {
public:
  HorizontallyAlignedFilter() {}
  virtual ~HorizontallyAlignedFilter() {}
  void FilterFrame(Frame* previous, Frame* next, size_t num_slots);
};

}  // namespace touch_noise_filter

#endif  // HORIZONTALLY_ALIGNED_FILTER_H__
