// Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "touch_noise_filter/single_frame_tap_filter.h"

#include <base/basictypes.h>

#include <cstddef>
#include <cstdio>

namespace touch_noise_filter {

namespace {
double kMaxTapDuration = 0.1;
}  // namespace {}

void SingleFrameTapFilter::FilterFrame(Frame* previous, Frame* current,
                                       size_t num_slots) {
  for (size_t i = 0; i < num_slots; i++) {
    Finger* prev = &previous->fingers_[i];
    Finger* cur = &current->fingers_[i];
    Log("got finger at %f in slot %zu", current->timestamp_, i);
    bool arrived = cur->tracking_id_ >= 0 && prev->tracking_id_ == -1;
    bool departing = cur->tracking_id_ == -1 && prev->tracking_id_ >= 0;
    if ((prev_arrived_ & (1ULL << i)) && departing &&
        (current->timestamp_ - previous->timestamp_ < kMaxTapDuration)) {
      cur->canceled_ = true;
      TouchNoiseFilterLog(TOUCH_NOISE_LOG_INFO, "CANCEL AT %f, %zu\n",
          current->timestamp_, i);
    }
    if (arrived)
      prev_arrived_ |= 1ULL << i;
    else
      prev_arrived_ &= ~(1ULL << i);
  }
}

}  // namespace touch_noise_filter
