// Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef INPUT_EVENT_FILTER_H__
#define INPUT_EVENT_FILTER_H__

#include <linux/input.h>
#include <stdint.h>

#include <base/memory/scoped_ptr.h>

#include "touch_noise_filter/filter.h"

// This class is the glue that connects xf86-input-evdev into the filter
// library. It passes in each input_event. After the SYN_REPORT is passed in
// it should call GetCanceledTouches() to find out which slots have been
// canceled.

// Non-X clients that just have finger data could call this object's filter_
// directly.

namespace touch_noise_filter {

class InputEventFilter {
public:
  InputEventFilter(NoiseFilter* filter);
  void HandleInputEvent(const struct input_event* ev);
  void GetCanceledTouches(uint64_t* slots_mask);

private:
  Frame* CurrentFrame() { return &frames_[0]; }
  Frame* PrevFrame() { return &frames_[1]; }

  Frame frames_[2];

  // bitmasks of which slots have touches
  uint64_t current_touching_;
  uint64_t prev_touching_;

  scoped_ptr<NoiseFilter> filter_;
  size_t current_slot_;
};

}  // namespace touch_noise_filter

#endif  // INPUT_EVENT_FILTER_H__
