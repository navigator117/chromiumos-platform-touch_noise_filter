// Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef FAR_APART_TAPS_FILTER_H__
#define FAR_APART_TAPS_FILTER_H__

#include "touch_noise_filter/filter.h"

namespace touch_noise_filter {

class FarApartTapsFilter : public NoiseFilter {
public:
  FarApartTapsFilter() {}
  virtual ~FarApartTapsFilter() {}
  void FilterFrame(Frame* previous, Frame* next, size_t num_slots);

private:
  struct Tap {
    Tap() : start_(0), x_pos_(0.0), y_pos_(0.0) {}
    Tap(double start, __s32 x_pos, __s32 y_pos)
        : start_(start), x_pos_(x_pos), y_pos_(y_pos) {}
    double start_;
    __s32 x_pos_, y_pos_;
  };

  // Tracks in progress taps in slots.
  Tap tracked_taps_[TOUCH_NOISE_MAX_SLOTS];
};

}  // namespace touch_noise_filter

#endif  // FAR_APART_TAPS_FILTER_H__
