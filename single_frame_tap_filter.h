// Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef SINGLE_FRAME_TAP_FILTER_H__
#define SINGLE_FRAME_TAP_FILTER_H__

#include "touch_noise_filter/filter.h"

namespace touch_noise_filter {

class SingleFrameTapFilter : public NoiseFilter {
public:
  SingleFrameTapFilter() : prev_arrived_(0) {}
  virtual ~SingleFrameTapFilter() {}
  void FilterFrame(Frame* previous, Frame* current, size_t num_slots);

private:
  uint64_t prev_arrived_;
};

}  // namespace touch_noise_filter

#endif  // SINGLE_FRAME_TAP_FILTER_H__
